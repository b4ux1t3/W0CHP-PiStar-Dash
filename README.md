## This is a fork

There has been a precedent of the original maintainer of this software having an opinion on whether some servers belong on the lists presented by this UI.

It is the _opinion_ of _this_ maintainer that that sort of opinionated design is damaging to the amateur radio hobby and community.

This maintainer is not making any judgements on the motivations or opinions of the original maintainer. The goal here is purely to allow users the alternative of accessing any server in the canonical list, not to condemn or malign any other parties' for their beliefs or views.

# The original README follows

**NOTE**: If you are reading this on a site other than its official *W0CHP/WPSD
.radio* domains (such as GitHub, CodeBerg, SourceHut,  etc.), then it is
**not** my official program / code, and is likely outdated, and is unsupported.
The canonical and official site for this program is: https://wpsd.radio/.

[Please don’t upload my code to GitHub](https://nogithub.codeberg.page) [![Please don't upload my code to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## About WPSD (`W0CHP-PiStar-Dash`)

WPSD ("W0CHP-PiStar-Dash") is a next-generation, digital voice software suite
for amateur radio use. It is used for personal hotspots and repeaters alike. It
supports M17, DMR, D-Star, Yaesu System Fusion (YSF/C4FM), P25, NXDN digital
voice modes & POCSAG data/paging.

WPSD began as a derivative/“fork” of the popular Pi-Star software, and has
evolved into its very own software and distribution over the years. It is
available as a disk image, and multiple platforms are supported.

## How to Install WPSD

For the sake of having to maintain only one authoritative document, it's all
documented here: http://wpsd.radio

